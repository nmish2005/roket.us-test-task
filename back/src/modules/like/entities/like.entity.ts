import {BelongsTo, Column, ForeignKey, Model, Table} from 'sequelize-typescript';
import {Article} from "../../article/entities/article.entity";

@Table
export class Like extends Model {
    @ForeignKey(() => Article)
    @Column
    articleId: number

    @BelongsTo(() => Article)
    article: Article
}
