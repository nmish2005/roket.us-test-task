import {Injectable} from "@nestjs/common";
import {InjectModel} from "@nestjs/sequelize";
import {Category} from "./entities/category.entity";

@Injectable()
export class CategoryService {
    constructor(@InjectModel(Category) private categoryModel: typeof Category) {
    }

    async getAll() {
        return this.categoryModel.findAll()
    }
}
