const API_URL = 'http://0.0.0.0:5611/v1'

export class APIRequests {
  static async getCats() {
      return (await fetch(API_URL + '/category')).json()
  }

  static async getArticlesByCategory(id: number, page = 1) {
    return (await fetch(API_URL + '/category/' + id + '?page=' + page)).json()
  }

  static async likeArticle(id: number) {
    return fetch(API_URL + '/article/' + id + '/like', {
      method: 'PATCH'
    })
  }
}
