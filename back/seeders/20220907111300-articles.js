const { faker } = require('@faker-js/faker');

'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    const articles = []

    for (let i = 0; i < 20; i++) {
        articles.push({
          image: faker.image.imageUrl(),
          title: faker.random.words(),
          shortDescription: faker.lorem.paragraphs(5),
          categoryId: faker.mersenne.rand(9, 4),
          createdAt: new Date,
          updatedAt: new Date
        })
    }

    await queryInterface.bulkInsert('Articles', articles, {});
  },

  async down(queryInterface, Sequelize) {
    /**
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
