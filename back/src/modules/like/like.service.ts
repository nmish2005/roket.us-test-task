import {Injectable} from "@nestjs/common";
import {InjectModel} from "@nestjs/sequelize";
import {Like} from "./entities/like.entity";

@Injectable()
export class LikeService {
    constructor(@InjectModel(Like) private likeModel: typeof Like) {
    }

    async like(articleId: number) {
        return this.likeModel.create({articleId})
    }
}
