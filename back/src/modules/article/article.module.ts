import {Module} from "@nestjs/common";
import {SequelizeModule} from "@nestjs/sequelize";
import {ArticleController} from "./article.controller";
import {ArticleService} from "./article.service";
import {Article} from "./entities/article.entity";
import {LikeModule} from "../like/like.module";
import {Like} from "../like/entities/like.entity";

@Module({
    imports: [SequelizeModule.forFeature([Article]), SequelizeModule.forFeature([Like]), LikeModule],
    controllers: [ArticleController],
    providers: [ArticleService],
    exports: [ArticleService]
})
export class ArticleModule {}
