import {Module} from '@nestjs/common';
import {SequelizeModule} from '@nestjs/sequelize';
import {ArticleModule} from './modules/article/article.module';
import {CategoryModule} from "./modules/category/category.module";
import {LikeModule} from "./modules/like/like.module";

@Module({
    imports: [
        SequelizeModule.forRoot({
            dialect: 'postgres',
            host: process.env.PG_HOST,
            port: Number(process.env.PG_PORT),
            username: process.env.PG_USER,
            password: process.env.PG_PASS,
            database: process.env.PG_DBNAME,
            autoLoadModels: true,
            synchronize: false
        }),

        CategoryModule,
        ArticleModule,
        LikeModule
    ]
})
export class AppModule {
}
