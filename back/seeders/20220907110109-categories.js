const { faker } = require('@faker-js/faker');

'use strict';

module.exports = {
    async up(queryInterface, Sequelize) {
        const cats = []

        while (cats.length < 10) {
            const newItem = faker.commerce.department()

            if (!cats.includes(newItem)) {
                cats.push(newItem)
            }
        }

        await queryInterface.bulkInsert('Categories', cats.map(name => ({name, createdAt: new Date, updatedAt: new Date})), {});
    },

    async down(queryInterface, Sequelize) {
        /**
         * await queryInterface.bulkDelete('People', null, {});
         */
    }
};
