import {Controller, Get, Param, Patch, Query} from "@nestjs/common";
import {ArticleService} from "./article.service";
import {LikeService} from "../like/like.service";

@Controller('/article')
export class ArticleController {
    constructor(private readonly articleService: ArticleService, private readonly likeService: LikeService) {
    }

    @Get('/:articleId')
    async getAllCats(@Param('articleId') articleId: number) {
        return await this.articleService.getById(articleId)
    }

    @Patch('/:articleId/like')
    async likeArticle(@Param('articleId') articleId: number) {
        return await this.likeService.like(articleId)
    }
}
