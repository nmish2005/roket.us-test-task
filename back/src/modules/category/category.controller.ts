import {Controller, Get, Param, Query} from "@nestjs/common";
import {CategoryService} from "./category.service";
import {ArticleService} from "../article/article.service";

@Controller('/category')
export class CategoryController {
    constructor(private readonly categoryService: CategoryService, private readonly articleService: ArticleService) {
    }

    @Get('/')
    async getAllCats() {
        return await this.categoryService.getAll()
    }

    @Get('/:catId')
    async getSingleCategory(@Param('catId') categoryId: number, @Query() {page}) {
        return await this.articleService.getByCategory(categoryId, page)
    }
}
