import {Column, HasMany, Model, Table} from 'sequelize-typescript';
import {Like} from "../../like/entities/like.entity";

@Table
export class Article extends Model {
    @Column
    title: string;

    @Column
    image: string;

    @Column
    shortDescription: string;

    @Column
    categoryId: number;

    @HasMany(() => Like)
    likes: Like[]
}
