import {Module} from "@nestjs/common";
import {SequelizeModule} from "@nestjs/sequelize";
import {CategoryController} from "./category.controller";
import {CategoryService} from "./category.service";
import {Category} from "./entities/category.entity";
import {ArticleModule} from "../article/article.module";

@Module({
    imports: [SequelizeModule.forFeature([Category]), ArticleModule],
    controllers: [CategoryController],
    providers: [CategoryService]
})
export class CategoryModule {}
