import {Injectable} from "@nestjs/common";
import {InjectModel} from "@nestjs/sequelize";
import {Article} from "./entities/article.entity";
import {Sequelize} from "sequelize-typescript";
import {Like} from "../like/entities/like.entity";

const PAGE_SIZE = 10;

@Injectable()
export class ArticleService {
    constructor(@InjectModel(Article) private articleModel: typeof Article, @InjectModel(Like) private likeModel: typeof Like) {
    }

    async getById(id: number) {
        return this.articleModel.findByPk(id, {
            attributes: [
                'title', 'image', 'shortDescription', 'createdAt',
                [Sequelize.literal('(SELECT COUNT("Likes"."id") FROM "Likes" WHERE "Likes"."articleId" = "Article"."id")'), 'likeCount']
            ],
            raw: true
        })
    }

    async getByCategory(categoryId: number, page: number = 1) {
        const paginatedArticles = await this.articleModel.findAndCountAll({
            where: {categoryId},
            attributes: [
                'id', 'title', 'image', 'shortDescription', 'createdAt',
                [Sequelize.literal('(SELECT COUNT("Likes"."id") FROM "Likes" WHERE "Likes"."articleId" = "Article"."id")'), 'likeCount']
            ],
            limit: PAGE_SIZE,
            offset: --page * PAGE_SIZE
        })

        return {
            totalPages: Math.ceil(paginatedArticles.count / PAGE_SIZE),
            limit: PAGE_SIZE,
            results: paginatedArticles.rows
        }
    }
}
